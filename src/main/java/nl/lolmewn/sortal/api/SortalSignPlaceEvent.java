/*
 *  Copyright 2013 Lolmewn <lolmewn@gmail.com>.
 */
package nl.lolmewn.sortal.api;

import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * @author Lolmewn <lolmewn@gmail.com>
 */
public class SortalSignPlaceEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled = false;

    private final Sign sign;
    private final Player player;

    public SortalSignPlaceEvent(Sign sign, Player player) {
        this.sign = sign;
        this.player = player;
    }

    public Sign getSign() {
        return sign;
    }

    public Player getPlayer() {
        return player;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean bln) {
        cancelled = bln;
    }

}
